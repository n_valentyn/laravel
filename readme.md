#Laravel 

Сервис Rest Api по работе с пользователями(Регистрация, Аутентификация)

Сервис доступный по локальному URL http://laravel.loc/api/v1/

##Доступные методы:      



- #user `metod POST` 

####Создает пользователя

####Параметры

Параметр      | Описание          | Обязательный
------------- | -------------     | -------------
first_name    | Имя пользователя  | да
last_name     | Фамилия           | да
email         | Электронная почта | да
password      | Пароль            | да
city          | Город             | нет   



- #user `metod GET` 

####Получает пользователя

####Параметры

Параметр       | Описание          | Обязательный
-------------  | -------------     | -----------
x-access-token | Токен авторизации | да


- #user `metod PUT` 

####Обновление данных пользователя

####Параметры 

Параметр       | Описание          | Обязательный
-------------- | ----------------- | --------
x-access-token | Токен авторизации | да
first_name     | Имя пользователя  | нет 
last_name      | Фамилия           | нет
email          | Электронная почта | нет
city           | Город             | нет



- #user/password `metod PUT` 

####Изменение пароля пользователя

####Параметры 

Параметр       | Описание          | Обязательный
-------------- | ----------------- | --------
x-access-token | Токен авторизации | да
old_password   | Старый пароль     | да
new_password   | Новый пароль      | да



- #user/password/restore `metod POST` 

####Создание заявки на восстановление пароля

####Параметры 

Параметр       | Описание          | Обязательный
-------------- | ----------------- | --------
email          | Электронная почта | да



- #user/password/restore `metod PUT` 

####Восстановление пароля

####Параметры 

Параметр       | Описание            | Обязательный
-------------- | -----------------   | --------
email          | Электронная почта   | да
password_token | Токен восстановления| да 
               | пароля              |
new_password   | Новый пароль        | да



- #session `metod POST` 

####Создает сессию пользователя

####Параметры

Параметр      | Описание          | Обязательный
------------- | -------------     | --------------
email         | Электронная почта | да
password      | Пароль            | да



- #session `metod GET` 

####Получает сессию пользователя

####Параметры

Параметр       | Описание          | Обязательный
-------------  | -------------     | -----------
x-access-token | Токен авторизации | да     



- #session `metod DELETE` 

####Удаляет сессию пользователя

####Параметры

Параметр       | Описание          | Обязательный
-------------  | -------------     | -----------
x-access-token | Токен авторизации | да  
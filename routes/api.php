<?php

use Illuminate\Http\Request;

// Группа маршрутов для работы с сервисом REST API версии 1
Route::group(['prefix' => 'v1'], function () {

    // Группа маршрутов для работы с пользователем
    Route::group(['prefix' => 'user'], function () {

        // Маршрут для добавления пользователя
        Route::post('/', [
            'uses' => 'UserController@creatAction',
            'middleware' => 'registration.validation'
        ]);

        // Маршрут для получения данных пользователя
        Route::get('/', [
            'uses' => 'UserController@getAction',
            'middleware' => ['token.validation', 'authorization']
        ]);

        // Маршрут для обновлении данных пользователя
        Route::put('/', [
            'uses' => 'UserController@updateAction',
            'middleware' => ['update.user.validation', 'authorization']
        ]);

        // Группа маршрутов для работы с паролем пользователя
        Route::group(['prefix' => 'password'], function () {

            // Маршрут для изменения пароля
            Route::put('/', [
                'uses' => 'UserController@updatePasswordAction',
                'middleware' => ['update.pass.validation', 'authorization']
            ]);


            // Группа маршрутов для восстановления пароля
            Route::group(['prefix' => 'restore'], function () {

                // Маршрут для создания заявки на восстановления пароля
                Route::post('/', [
                    'uses' => 'UserController@creatRestorePasswordAction',
                    'middleware' => 'creat.restore.pass.validation'
                    ]);

                // Маршрут для восстановления пароля
                Route::put('/', [
                    'uses' => 'UserController@restorePasswordAction',
                    'middleware' => 'restore.pass.validation'
                    ]);
            });
        });
    });

    // Группа маршрутов для работы с сессией пользователя
    Route::group(['prefix' => 'session'], function () {

        // Маршрут для создания сессии
        Route::post('/', [
            'uses' => 'SessionController@creatAction',
            'middleware' => 'session.validation'
        ]);

        // Маршрут для получения сессии
        Route::get('/', [
            'uses' => 'SessionController@getAction',
            'middleware' => ['token.validation', 'authorization']
        ]);

        // Маршрут для удаления сессии
        Route::delete('/', [
            'uses' => 'SessionController@deleteAction',
            'middleware' => ['token.validation', 'authorization']
        ]);
    });
});

<?php

namespace App\ModelInterface;

use App\Session\Contract\SessionContract;

/**
 * Интерфейс модели User
 *
 * @author valentyn
 */
interface UserInterface
{
    public function getInfo():array;

    //  public function getAdverts();                          // :[]AdvertInterface
//
    //  public function getFavorites();                        // :[]AdvertInterface
    
    public function changePassword(string $newPassword);   
    
    public function logout(SessionContract $session);      

    public function updateProfile(array $profile);         
}

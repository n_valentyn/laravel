<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;
use App\Addition\Session\Contract\SessionContract;
use Illuminate\Support\Facades\Hash;

/**
 *
 * Контроллер реализует работу с сессией пользователя
 */
class SessionController extends Controller
{

    /**
     * Создание новой сессии пользователя
     *
     * @param Request $request, SessionContract $session
     * @return Response
     */
    public function creatAction(Request $request, SessionContract $session)
    {

        // Получение пользователя по переданому email
        $user = User::where('email', $request->email)->first();
        
        // Если хэш пароля неправильный
        if (!Hash::check($request->password, $user->password)) {
            $errors[] = 'Invalid password';
            // Ответ об неправильном пароле
            return response()->tempJson(422, $request->post(), $errors);
        }

        // Создание сессии
        $session->createSession($user);

        $data = [
            'email' => $request->email,
            'x-access-token' => $session->getToken()
        ];
        // Ответ об успешном создании сессии
        return response()->tempJson(200, $data);
    }

    /**
     * Получение сессии пользователя
     *
     * @param SessionContract $session
     * @return Response
     */
    public function getAction(SessionContract $session)
    {
        $data = [
            'x-access-token' => $session->getToken(),
            'user' => $session->getUser()->getInfo()
        ];
        // Ответ об успешном создании сессии
        return response()->tempJson(200, $data);
    }
    
    /**
     * Удаление сессии пользователя
     *
     * @param SessionContract $session
     * @return Response
     */
    public function deleteAction(SessionContract $session)
    {
        // Удаление сессии
        $session->expire();

        // Ответ об успешном удалении сессии
        return response()->tempJson(200);
    }
}

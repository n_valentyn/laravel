<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Addition\Session\Contract\SessionContract;
use Illuminate\Support\Facades\Hash;

/**
 *
 * Контроллер реализует работу с пользователями
 */
class UserController extends Controller
{

    /**
     * Метод создает нового пользователя
     *
     * @param Request $request
     * @return response
     */
    public function creatAction(Request $request)
    {
        // Поля которые нужны для создания пользователя
        $filds = ['first_name', 'last_name', 'email', 'password', 'city'];

        // Получение массива нужных полей пользователя из запроса
        $data = $request->only($filds);

        // Хеширование пароля
        $data['password'] = Hash::make($data['password']);

        // Создание пользователя в базе данных
        User::create($data);

        // Ответ об успешном создании пользователя
        return response()->tempJson(200, $request->only($filds));
    }

    /**
     * Получение пользователя
     *
     * @param Request $request
     * @return Rseponse
     */
    public function getAction(Request $request, SessionContract $session)
    {
        // Запись данных в массив
        $data = [
            'x-access-token' => $request->header('x-access-token'),
            'user' => $session->getUser()->getInfo()
        ];

        // Ответ об успешном получении пользователя
        return response()->tempJson(200, $data);
    }

    /**
     * Обновление данных пользователя
     *
     * @param Request $request
     * @return Response
     */
    public function updateAction(Request $request, SessionContract $session)
    {
        // Запись переданых данных в массив
        $data = array_merge(
            ['x-access-token' => $request->header('x-access-token')],
            $request->all()
        );

        // Поля которые нужны для обновления данных пользователя
        $filds = ['first_name', 'last_name', 'email', 'city'];

        // Получение массива нужных полей пользователя из запроса
        // и удаления пустих елементов
        $profileData = array_filter($request->only($filds));

        //Если массив полей пустой
        if (count($profileData) == 0) {
            $errors[] = 'No data for updating';
            
            // Ответ об отсутствии данных для обновления
            return response()->tempJson(422, $data, $errors);
        }

        // Получения модели пользователя и обновление её данных
        $user = $session->getUser()->updateProfile($profileData);

        //Обновление данных в сессии
        $session->updateSession($user);

        // Ответ об успешном изменении данных пользователя
        return response()->tempJson(200, $data);
    }
    
    /**
     * Изменения пароля пользователя
     *
     * @param Request $request
     * @return Response
     */
    public function updatePasswordAction(Request $request, SessionContract $session)
    {
        // Запись переданых данных в массив
        $data = array_merge(
            ['x-access-token' => $request->header('x-access-token')],
            $request->all()
        );
       
        // Получения модели пользователя из БД
        $user = User::find($session->getUser()->getInfo()['id']);
        
        // Если хэш старого пароля неправильный
        if (!Hash::check($data['old_password'], $user->password)) {
            $errors[] = 'Invalid old password';
            
            // Ответ об неудачи изменения пароля
            return response()->tempJson(422, $data, $errors);
        }

        // обновление пароля пользователя
        $user->changePassword($data['new_password']);
        
        //Обновление данных в сессии
        $session->updateSession($user);
        
        // Ответ об успешном изменении пароля
        return response()->tempJson(200);
    }
    
    /**
     * Создание заявки на восстановление пароля
     *
     * @param Request $request
     * @return Response
     */
    public function creatRestorePasswordAction(Request $request)
    {
        // Получение пользователя по переданому email
        $user = User::where('email', $request->email)->first();

        // Создание токена на восстановление пароля
        $token = Hash::make($user->email . time());
        
        // Сохранение токена
        $user->password_token = $token;
        $user->save();
        
        //Отправка токена на электронную почту
        $data[] = [
            'email' => $request->email,
            'email_message' => $token        //для тестирования
            ];
        
        // Ответ об успешном создании заявки
        return response()->tempJson(200, $data);
    }
    
    /**
     * Восстановление пароля
     *
     * @param Request $request
     * @return Resonse
     */
    public function restorePasswordAction(Request $request)
    {
        // Получение пользователя по переданому email
        $user = User::where('email', $request->email)->first();
        
        // Если токен восстановления пароля неверный
        if ($user->password_token != $request->input('password_token')) {
            $errors[] = 'There is no such request for password recovery';
            
            // Ответ об неудачи изменения пароля
            return response()->tempJson(422, $request->all(), $errors);
        }
        
        // обновление пароля и удаление заявки на восстановление пароля
        $user->changePassword($request->input('new_password'));
        
        // Ответ об успешном изменении пароля
        return response()->tempJson(200);
    }
}

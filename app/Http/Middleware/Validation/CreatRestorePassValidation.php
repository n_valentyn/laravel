<?php

namespace App\Http\Middleware\Validation;

use App\Http\Middleware\Validation\AbstractValidation\Validation;

class CreatRestorePassValidation extends Validation
{
    public function rules(): array
    {
        return [
            'email' => 'required|email|min:3|max:255|exists:users,email'
        ];
    }
}

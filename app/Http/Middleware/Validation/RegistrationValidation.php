<?php

namespace App\Http\Middleware\Validation;

use App\Http\Middleware\Validation\AbstractValidation\Validation;

/**
 * Description of RegistrationValidation
 *
 * @author Admin
 */
class RegistrationValidation extends Validation
{
    public function rules(): array
    {
        return [
            'first_name' => 'required|string|min:3|max:255',
            'last_name' => 'required|string|min:3|max:255',
            'email' => 'required|email|min:3|max:255|unique:users,email',
            'password' => 'required|min:8|max:255',
            'city' => 'sometimes|string|min:3|max:255|nullable'
        ];
    }
}

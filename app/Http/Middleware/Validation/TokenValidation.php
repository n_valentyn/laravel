<?php

namespace App\Http\Middleware\Validation;

use App\Http\Middleware\Validation\AbstractValidation\Validation;

class TokenValidation extends Validation
{
    public function rules(): array
    {
        return [
            'x-access-token' => 'required|string|min:50|max:255'
        ];
    }
}

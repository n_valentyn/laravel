<?php

namespace App\Http\Middleware\Validation\AbstractValidation;

use Closure;
use Validator;

abstract class Validation
{
    abstract public function rules(): array;

    public function handle($request, Closure $next)
    {
        // Запись переданых данных в массив
        $arrayRequest = array_merge(
                ['x-access-token' => $request->header('x-access-token')],
                $request->all()
        );

        // Удаление пустых значений
        $arrayRequest = array_filter($arrayRequest);

        // Проверка полей
        $validation = Validator::make($arrayRequest, $this->rules());

        // Если есть ошибки проверки полей
        if ($validation->fails()) {
            // Ответ об неудачи
            return response()->tempJson(422, $arrayRequest, $validation->messages()->toArray());
        }

        return $next($request);
    }
}

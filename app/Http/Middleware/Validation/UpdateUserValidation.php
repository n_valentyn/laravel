<?php

namespace App\Http\Middleware\Validation;

use App\Http\Middleware\Validation\AbstractValidation\Validation;

class UpdateUserValidation extends Validation
{
    public function rules(): array
    {
        return [
            'first_name' => 'sometimes|nullable|string|min:3|max:255',
            'last_name' => 'sometimes|nullable|string|min:3|max:255',
            'email' => 'sometimes|nullable|email|min:3|max:255|unique:users,email',
            'city' => 'sometimes|nullable|string|min:3|max:255|',
            'x-access-token' => 'required|string|min:50|max:255'
        ];
    }
}

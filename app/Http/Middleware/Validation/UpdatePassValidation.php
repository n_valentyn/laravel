<?php

namespace App\Http\Middleware\Validation;

use App\Http\Middleware\Validation\AbstractValidation\Validation;

class UpdatePassValidation extends Validation
{
    public function rules(): array
    {
        return [
            'old_password' => 'required|min:8|max:255',
            'new_password' => 'required|min:8|max:255',
            'x-access-token' => 'required|string|min:50|max:255'
        ];
    }
}

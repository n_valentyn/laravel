<?php

namespace App\Http\Middleware\Validation;

use App\Http\Middleware\Validation\AbstractValidation\Validation;

class SessionValidation extends Validation
{
    public function rules(): array
    {
        return [
            'email' => 'required|email|min:3|max:255|exists:users,email',
            'password' => 'required|min:8|max:255'
        ];
    }
}

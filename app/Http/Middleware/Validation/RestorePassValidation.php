<?php

namespace App\Http\Middleware\Validation;

use App\Http\Middleware\Validation\AbstractValidation\Validation;

class RestorePassValidation extends Validation
{
    public function rules(): array
    {
        return [
            'email' => 'required|email|min:3|max:255|exists:users,email',
            'password_token' => 'required|string|min:50|max:255',
            'new_password' => 'required|min:8|max:255'
        ];
    }
}

<?php

namespace App\Http\Middleware;

use Closure;
use App\Addition\Session\Contract\SessionContract;

class Authorization
{
    private $session;
    
    public function __construct(SessionContract $session)
    {
        $this->session = $session;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Если пользователь cессии не найден
        if ($this->session->find($request->header('x-access-token')) == false) {
            $data = array_merge(
                ['x-access-token' => $request->header('x-access-token')],
                $request->all()
            );
            $errors[] = 'Session not found';
            
            // Ответ об неудачи
            return response()->tempJson(401, $data, $errors);
        }
        
        return $next($request);
    }
}

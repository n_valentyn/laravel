<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ModelInterface\UserInterface;
use Illuminate\Support\Facades\Hash;
use App\Session\Contract\SessionContract;

/**
 * Модель пользователя
 */
class User extends Model implements UserInterface
{
    /**
     * Связанная с моделью таблица.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * Атрибуты, для которых разрешено массовое назначение.
     *
     * @var array
     */
    protected $fillable = ['first_name', 'last_name', 'email', 'password', 'city'];
    
    /**
    * Атрибуты, которые должны быть невидимы для массива.
    *
    * @var array
    */
    protected $hidden = ['password', 'password_token'];
    
    public function changePassword(string $newPassword)
    {
        $this->password = Hash::make($newPassword);
        $this->password_token = null;
        $this->save();
        return $this;
    }

    public function getInfo():array
    {
        return $this->toArray();
    }

    public function logout(SessionContract $session)
    {
        $session->expire();
    }

    public function updateProfile(array $profile)
    {
        $this->fill($profile);
        $this->save();
        
        return $this;
    }
}

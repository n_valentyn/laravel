<?php

namespace App\Addition\Session;

use App\Addition\Session\Contract\SessionContract;
use App\ModelInterface\UserInterface;
use Illuminate\Support\Facades\Hash;
use Cache;

/**
 * Description of RedisSession
 *
 * @author valentyn
 */
class RedisSession implements SessionContract
{

    /**
     * Токен сессии
     *
     * @var string
     */
    protected $token;

    /**
     * Пользователь сессии
     *
     * @var string
     */
    protected $user;

    /**
     * Cоздание сессии пользователя
     *
     * @param UserInterface $user
     * @return SessionContract
     */
    public function createSession(UserInterface $user): SessionContract
    {
        $this->user = $user;
        $this->token = Hash::make($user->getInfo()['email'] . time());

        $this->store();

        return $this;
    }

    /**
     * Обновление сессии пользователя
     *
     * @param UserInterface $user
     * @return SessionContract
     */
    public function updateSession(UserInterface $user): SessionContract
    {
        $this->user = $user;
        
        $this->store();

        return $this;
    }
    
    /**
     * Удаление сессии пользователя
     *
     */
    public function expire()
    {
        Cache::forget($this->token);
    }

    /**
     * Поиск сессии
     *
     * @param string $token
     * @return SessionContract
     */
    public function find(string $token): bool
    {
        $this->token = $token;
        
        if (Cache::has($token)) {
            $obj = unserialize(Cache::get($token));
            
            if ($obj instanceof UserInterface) {
                $this->user = $obj;
                
                $this->prolong();
                
                return true;
            }
        }

        return false;
    }

    /**
     * Получение пользователя сессии
     *
     * @return UserInterface
     */
    public function getUser(): ?UserInterface
    {
        return $this->user;
    }
    
    /**
     * Получение токена сессии
     *
     * @return UserInterface
     */
    public function getToken(): string
    {
        return $this->token;
    }

    /**
     * Продление сессии пользователя
     *
     */
    public function prolong()
    {
        Cache::put($this->token, serialize($this->user), 3 * 60);
    }

    /**
     * Сохранение сессии
     *
     */
    private function store()
    {
        Cache::put($this->token, serialize($this->user), 3 * 60);
    }
}

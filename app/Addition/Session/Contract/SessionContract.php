<?php

namespace App\Addition\Session\Contract;

use App\ModelInterface\UserInterface;

/**
 *
 * @author valentyn
 */
interface SessionContract
{
    public function find(string $token): bool;
    
    public function createSession(UserInterface $user): SessionContract;
    
    public function updateSession(UserInterface $user): SessionContract;
    
    public function prolong();                           // :void;
    
    public function expire();                            // :void;
    
    public function getUser(): ?UserInterface;
    
    public function getToken(): string;
}

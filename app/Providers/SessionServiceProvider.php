<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Addition\Session\RedisSession;
use App\Addition\Session\Contract\SessionContract;

class SessionServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SessionContract::class, function ($app) {
            return new RedisSession();
        });
    }
}

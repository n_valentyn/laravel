<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Response;

class AppServiceProvider extends ServiceProvider
{

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /**
         * Шаблон ответа Json
         */
        Response::macro('tempJson', function (int $code, array $data = [],array $errors = []) {
            
            //Получение статуса из кода ошибки
            $status = ($code == 200)? true : false ;

            //Шаблон ответа
            return response()->json([
                        'status' => $status,
                        'code' => $code,
                        'data' => $data,
                        'errors' => $errors
            ]);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}

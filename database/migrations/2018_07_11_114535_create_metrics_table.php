<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMetricsTable extends Migration
{
    /**
     * Run the migrations.
     * Create new table metrics
     *
     * @return void
     */
    public function up()
    {
        Schema::create('metrics', function (Blueprint $table) {
            $table->increments('id');
            $table->ipAddress('ip_address')->nullable();
            $table->timestamps();
            $table->string('referer', 255)->nullable();
            $table->string('user_agent', 255)->nullable();
            $table->integer('advert_id')->unsigned();
            
            $table->foreign('advert_id')
                    ->references('id')
                    ->on('adverts')
                    ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('metrics');
    }
}
